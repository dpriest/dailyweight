
<cfquery name="checkUser">
   SELECT email, id
   FROM users
   WHERE email = 
	<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.email#">
   AND password = 
	<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.password#">
</cfquery>
 
	<div class="container">
	   	<cfif checkUser.recordCount eq 1>
	   		<cfset session.isLoggedIn = "YES">	
	    	<cfset session.email = "#checkUser.email#">
			<cfset session.day = 5>
			<cfset session.weight = ArrayNew(2)>
			<cfset session.weight[1][1] = '01/10/2013'>
			<cfset session.weight[1][2] = 8>
			<cfset session.weight[2][1] = '01/11/2013'>
			<cfset session.weight[2][2] = 10>
			<cfset session.i = 1>
			<cfset session.id = '#checkUser.id#'>

			<h1> You are now logged in! </h1>
			<a href="daily.cfm">Vist your Profile</a>
			<br>
			<a href="logout_success.cfm">Logout</a>
	   
	   <cfelse> 
	     	<h3>Sorry, that username and password are not recognized.</h3>
	     	<h4>Please try again.</h4>
	     	<cfinclude template="login.cfm">
	    	<cfabort>
	   </cfif>
	</div>