<cfcomponent output="false">
		
  	<cfset this.name="DailyWeight">
  	<cfset this.sessionManagement=true>  
 	<cfset this.dataSource="dailyweight">

	<cffunction name="onApplicationStart" output="false" returnType="void">

    	<cfset application.appName = "Daily Weight">
  
  	</cffunction>
  	
  	<cffunction name="onRequestStart" output="true" returnType="void">
  
    	<cfinclude template="header.cfm">
    
  	</cffunction>
  	
  	<cffunction name="increment_i" output="false" returnType="void">
	 
	 	<cfset session.i = session.i + 1> 	
	 
	</cffunction>

</cfcomponent>